const path = require('path');
const projectRoot = path.resolve(__dirname, '..');
const merge = require('webpack-merge');

const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');

module.exports = merge(require('./webpack.base'), {
  mode: 'development',
  entry: path.join(projectRoot, '/src/entry-server.js'),

  //This allows webpack to handle dynamic imports in a Node-appropriate
  //fashion, and also tells `vue-loader` to emit server-oriented code when
  //compiling Vue components.
  target: 'node',
  //For bundle renderer source map support
  devtool: 'source-map',

  //This tells the server bundle to use Node-style exports
  devServer: {
    inline: false,
    contentBase: './dist',
  },
  plugins: [
    new VueSSRServerPlugin(),
  ],
  output: {
    libraryTarget: 'commonjs2',
  },
});
