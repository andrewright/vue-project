const path = require('path');
const projectRoot = path.resolve(__dirname, '..');
const merge = require('webpack-merge');

const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');

module.exports = merge(require('./webpack.base'), {
  mode: 'development',
  entry: path.join(projectRoot, '/src/entry-client.js'),
  devServer: {
    inline: false,
    contentBase: './dist',
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        _vendors: {
          name: '_vendors',
          test: /node_modules/,
          chunks: 'initial',
          priority: 10,
          enforce: true,
        },
        commons: {
          name: 'commons',
          chunks: 'initial',
          enforce: true,
        },
      },
    },
  },
  plugins: [
    //This plugins generates `vue-ssr-client-manifest.json` in the
    //output directory.
    new VueSSRClientPlugin(),
  ],
  output: {
    filename: '[name].js'
  },
});
