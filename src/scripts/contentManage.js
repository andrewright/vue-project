import ContentService from './services/ContentService.js';
import AuthenticationService from './services/AuthenticationService.js';

class ContentManage {
  constructor() {
    this.$el = document.getElementById('contentManageElement');
    const token = this.$el.getAttribute('auth-token');
    this.siteLang = this.$el.getAttribute('site-lang');
    if (token !== null && token.length > 1) {
      AuthenticationService.authed({ token }).then((response) => {
        if (response.data.response === 'OK') {
          this.auth = true;
        } else {
          this.auth = false;
        }
        this.editContent();
      });
    }
    this.token = token;
    this.allowed = ['p', 'a', 'i', 'b', 'br', 'em', 'strong', ''];
  }

  editContent() {
    document.querySelectorAll('[data-unit]')
      .forEach((unit) => {
        const data = [];
        data.unit = unit.getAttribute('data-unit');
        data.lang = this.siteLang;
        this.error = null;
        try {
          this.replaceContent(data);
        } catch (error) {
          console.log(error);
          this.error = error.response.data.error;
        }
        return false;
      });
    if (!this.auth) return;
    document.querySelectorAll('[data-key]')
      .forEach((element) => {
        let timeoutCounter;
        element.addEventListener('click', (obj) => {
          obj.preventDefault();
          obj.stopPropagation();
          obj.stopImmediatePropagation();
          element.classList.add('content-management-select');
          timeoutCounter = setTimeout(() => {
            clearTimeout(timeoutCounter);
            if (obj.target.href) {
              window.location.href = obj.target.href;
            }
          }, 400);
        });
        element.addEventListener('dblclick', (obj) => {
          obj.stopPropagation();
          obj.stopImmediatePropagation();
          clearTimeout(timeoutCounter);
          clearTimeout(timeoutCounter - 1);
          this.initiatePopup(obj);
        });
        element.addEventListener('mouseover', (obj) => {
          obj.stopPropagation();
          obj.stopImmediatePropagation();
          element.classList.add('content-management-mouseover');
          document.querySelector('.content-management-hint')
            .classList
            .add('active');
        });
        element.addEventListener('mouseout', (obj) => {
          obj.stopPropagation();
          obj.stopImmediatePropagation();
          element.classList.remove('content-management-mouseover');
          element.classList.remove('content-management-select');
          document.querySelector('.content-management-hint')
            .classList
            .remove('active');
        });
      });
  }

  initiatePopup(obj) {
    this.srcElement = this.findParent(obj, 'data-key');
    const popupModalClassName = 'content-management-popup';
    const popupModalObj = document.querySelector(`.${popupModalClassName}`);
    const modalBody = document.querySelector(`.${popupModalClassName} .modal-body`);
    const modalBodyHTML = modalBody.innerHTML;
    modalBody.innerHTML = modalBodyHTML;
    const objSrcElementInnerHTML = this.srcElement.innerHTML.replace(/data-v.+?" /ig, '');

    document.querySelector(`.${popupModalClassName}`).classList.add('activate');
    document.querySelector(`.${popupModalClassName} .message-area`).innerHTML = '';
    const popupModalTextareaClassName = `${popupModalClassName}-textarea`;
    let popupModalTextarea = document.querySelector(`.${popupModalTextareaClassName}`);
    const popupModalTextareas = document.querySelectorAll(`.${popupModalTextareaClassName}`);
    this.current = this.findParent(obj, 'data-unit');
    const params = { key:this.srcElement.dataset.key, unit:this.current.getAttribute('data-unit'), lang: '*' };
    ContentService.readUnitKey(params).then((response) => {
      response.data.forEach((el) => { // assigning textareas
        popupModalTextareas.forEach((popupModalTextarea) => {
          if (popupModalTextarea.dataset.id === el.lang) {
            popupModalTextarea.innerHTML = el.text;
          }
        });
      });
      const thisLocal = this;

      // class adding functionality managed in the popup component
      popupModalTextareas.forEach(function(popupModalTextarea) {
        popupModalTextarea.classList.remove('active');
        const { id } = popupModalTextarea.dataset;
        if (thisLocal.siteLang === id) {
          popupModalTextarea.innerHTML = objSrcElementInnerHTML;
          popupModalTextarea.classList.add('active');
        }
      });
      const popupModalConfirmBtnClassName = `${popupModalClassName}-confirm-btn`;
      const popupModalConfirmBtn = document.querySelector(`.${popupModalConfirmBtnClassName}`);
      popupModalConfirmBtn.addEventListener('click', (event) => {
        event.stopImmediatePropagation();
        event.preventDefault();
        document.querySelector('.message-area').innerHTML = '';
        this.prepareText({
          obj,
          popupModalTextarea,
          popupModalClassName,
          popupModalTextareaClassName
        });
      });
    });
  }

  prepareText({ obj, popupModalTextarea, popupModalClassName, popupModalTextareaClassName }) {
    const data = [];
    let okflag = false;
    try {
      const page = this.findParent(obj, 'data-page');

      // Limiting entry tags
      const popupModalTextareas = document.querySelectorAll(`.${popupModalTextareaClassName}`);
      popupModalTextareas.forEach((popupModalTextarea) => {
        const { id } = popupModalTextarea.dataset;
        let match;
        let index = 0;
        let text = popupModalTextarea.value;

        while (match = text.match(/\<([^\/]+?)[ \>\/]/, index)) {
          index = match.index;
          text = text.substring(match.index + 1, text.length);
          if (this.allowed.indexOf(match[1]) === -1) {
            alert(`Tag ${match[1]} is not allowed`);
            return;
          }
        }
        if (page !== undefined) {
          data.unit = this.current.getAttribute('data-unit');
          const pageless = this.current.getAttribute('data-pageless') !== null;
          data.page = pageless ? 0 : page.getAttribute('data-page');
          data.key = this.srcElement.dataset.key;
          popupModalTextarea = document.querySelector(`.${popupModalTextareaClassName}[data-id="${id}"]`);
          data.text = popupModalTextarea.value;
          data.lang = id;
          data.method = 'save';
          // disable length test before availability check
          // if (data.text.length > 0) {
          const message = this.updateContent(data);
          if (message !== false) {
            document.querySelector('.message-area').innerHTML = message;
            document.querySelector('.message-area').classList.add('danger');
          } else {
            okflag = true;
          }
          okflag = true;
          //} else {
          //  document.querySelector('.message-area').innerHTML = 'The text field should be at least one character.';
          //  document.querySelector('.message-area').classList.add('danger');
          //  okflag = false;
          //}
        }
      });
    } catch (error) {
      document.querySelector('.message-area').innerHTML = error;
      document.querySelector('.message-area').classList.add('danger');
    }
    if (okflag) {
      document.querySelector(`.${popupModalClassName}`)
        .classList
        .remove('activate');
      return false;
    }
  }

  updateContent(data) {
    this.error = null;
    let dataCopy = {
      page: data.page,
      unit: data.unit,
      key: data.key,
      text: data.text,
      lang: data.lang,
    };
    try {
      const thisLocal = this;
      if (dataCopy.text.length > 0) {
        const response = ContentService.update({
          page: dataCopy.page,
          unit: dataCopy.unit,
          key: dataCopy.key,
          content: dataCopy.text,
          lang: dataCopy.lang,
          token: this.token,
        }).then((response) => {
          if (response.data) {
            if (response.data.error) {
              console.log(response.data.error);
              alert(response.data.error);
            }
          }
          thisLocal.replaceContent(dataCopy);
        });
      };
    } catch (error) {
      console.log(error);
      this.error = error.response.data.error;
    }
  }

  replaceContent(data) {
    this.use = '';
    ContentService.readUnit({
      unit: data.unit,
      lang: data.lang,
    })
      .then((response) => {
        const content = [];
        content.push(response.data);
        document.querySelectorAll(`[data-unit=${data.unit}] [data-key]`)
          .forEach((internal) => {
            content[0].forEach((item) => {
              if (item.key === internal.getAttribute('data-key') && item.lang === this.siteLang) {
                internal.innerHTML = item.text;
              }
            });
          });
      });
  }

  findParent(obj, param) {
    let current = obj.srcElement;
    do {
      current = current.parentElement;
      if (current === null) return obj.srcElement;
    } while (!current.getAttribute(param));
    return current;
  }
}

const contentManage = new ContentManage();
