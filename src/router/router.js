import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import P404 from '../components/404.vue';
import Blank from '../components/Blank.vue';
import NotFound from '../components/NotFound.vue';
import Admin from '../components/Admin.vue';
import HomePubl from '../components/HomePubl.vue';
import HomeTrial from '../components/HomeTrial.vue';
import Landing from '../components/Landing.vue';
import LeaveRequest from '../components/Pages/LeaveRequest.vue';
import SamplePage from '../components/Pages/SamplePage.vue';
import Get4TimesMore from '../components/Pages/Get-4-times-more.vue';
import Test1 from '../components/Pages/test1.vue';
import Test2 from '../components/Pages/test2.vue';

Vue.use(Router);
Vue.use(Meta, {
  keyName: 'metaInfo', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-vue-meta', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-vue-meta-server-rendered', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'vmid', // the property name that vue-meta uses to determine whether to overwrite or append a tag
});

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      { path: '*', component: P404, props: { language: 'en' } },
      { path: '/admin', component: Admin, name: 'admin' },
      { path: '/', name: 'home-en', component: HomePubl, props: { language: 'en' } },
      { path: '/try', name: 'trial-en', component: HomeTrial, props: { language: 'en' } },
      { path: '/pl-en/get-4-times-more', name: 'get-4-times-more-en', component: Get4TimesMore, props: { language: 'en' } },
      { path: '/pl-pl/get-4-times-more', name: 'get-4-times-more-pl', component: Get4TimesMore, props: { language: 'pl' } },
      { path: '/nl-nl/get-4-times-more', name: 'get-4-times-more-nl', component: Get4TimesMore, props: { language: 'nl' } },
      { path: '/en/sample', name: 'sample-en', component: SamplePage, props: { language: 'en' } },
      { path: '/pl/sample', name: 'sample-pl', component: SamplePage, props: { language: 'pl' } },
      
      { path: '/en/test', name: 'test-en', component: Test1, props: { language: 'en' } },
      { path: '/pl/test', name: 'test-pl', component: Test1, props: { language: 'pl' } },
  
      { path: '/en/test2', name: 'test2-en', component: Test2, props: { language: 'en' } },
      { path: '/pl/test2', name: 'test2-pl', component: Test2, props: { language: 'pl' } },
      {
        path: '/en/landing/',
        name: 'landing',
        component: Landing,
        props: { language: 'en' },
      },
      {
        path: '/en/leave-request/',
        name: 'leave-request',
        component: LeaveRequest,
        props: { language: 'en' },
      },
    ],
  });
}
