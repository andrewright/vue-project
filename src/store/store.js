import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import * as Cookies from 'js-cookie';

// Assume we have a universal API that returns Promises
// and ignore the implementation details
import { fetchItem } from '../../api';

Vue.use(Vuex);

const cookieStorage = {
  getItem(key) {
    return Cookies.getJSON(key);
  },
  setItem(key, value) {
    return Cookies.set(key, value, {
      expires: 3,
      secure: false,
    });
  },
  removeItem(key) {
    return Cookies.remove(key);
  },
};

const plugins = [];

plugins.push(createPersistedState({
  storage: cookieStorage,
  getState: cookieStorage.getItem,
  setState: cookieStorage.setItem,
}));

export function createStore() {
  return new Vuex.Store({
    state: {
      auth: false,
      token: null,
      items: {},
      user: {
        country: null,
      },
    },
    actions: {
      fetchItem({ commit }, id) {
        // return the Promise via `store.dispatch()` so that we know
        // when the data has been fetched
        return fetchItem(id)
          .then((item) => {
            commit('setItem', {
              id,
              item,
            });
          });
      },
      setToken({ commit }, token) {
        commit('setToken', token);
      },
      removeToken({ commit }) {
        commit('removeToken');
      },
      setUserCountry({ commit }, country) {
        commit('setUserCountry', country);
      },
    },
    mutations: {
      setItem(state, { id, item }) {
        Vue.set(state.items, id, item);
      },
      setToken(state, token) {
        state.token = token;
        state.auth = !!token;
      },
      removeToken(state) {
        state.token = null;
        state.auth = false;
      },
      setUserCountry(state, countryParam) {
        let country = countryParam;
        if (typeof(country) === 'string') {
          country = country.toLowerCase();
        }
        state.user.country = country;
      },
    },
    getters: {
      setToken: state => state.auth,
    },
    strict: false,
    plugins,
  });
}
