const fs = require('fs');
const path = require('path');

const projectRoot = path.resolve(__dirname, '../../..');
const config = require(path.join(projectRoot, '/config'));
const Sequelize = require('sequelize');

const db = {};

const sequelize = new Sequelize(
  config.db.database,
  config.db.username,
  config.db.password,
  config.db.options,
);

fs
  .readdirSync(__dirname)
  .filter(file => file !== 'index.js')
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

// db['ContentUnit'].hasMany(db['Content'])
// db['Content'].belongsTo(db['ContentUnit']) //{foreignKey: 'user_id'}
// db['Language'].hasMany(db['Content'])

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
