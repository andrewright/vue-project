module.exports = (sequelize, DataTypes) => sequelize.define('ContentUnit', {
  unit: {
    type: DataTypes.STRING,
    unique: false,
  },
  page_id: {
    type: DataTypes.INTEGER,
    references: {
      model: 'ContentPage',
      key: 'id',
    },
  },
});
