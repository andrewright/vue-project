// const {ContentUnit} = require('../models/')

module.exports = (sequelize, DataTypes) => {
  const Content = sequelize.define('Content', {
    lang: {
      type: DataTypes.STRING(2),
      unique: 'unit_key',
      references: {
        model: 'Language',
        key: 'id',
      },
    },
    unit_id: {
      type: DataTypes.INTEGER,
      unique: 'unit_key',
      references: {
        model: 'ContentUnit',
        key: 'id',
      },
    },
    key: {
      type: DataTypes.STRING,
      unique: 'unit_key',
    },
    init: {
      type: DataTypes.BOOLEAN,
      unique: 'unit_key',
      defaultValue: true,
    },
    text: DataTypes.TEXT,
  }, { tableName: 'Content' });

  Content.readUnit = async function (lang, unit, init) {
    const content = await sequelize.query(
      `SELECT * FROM Content c INNER JOIN ContentUnits cu ON cu.id = c.unit_id WHERE lang = '${lang}' AND cu.unit = '${unit}' AND c.init = '${init || 0}'`,
    );

    if (!content[0]) {
      return 'Details are incorrect';
    }

    return content[0];
  };

  return Content;
};
