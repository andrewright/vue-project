module.exports = (sequelize, DataTypes) => {
  const Language = sequelize.define('Language', {
    id: {
      type: DataTypes.STRING(2),
      primaryKey: true,
    },
    name_eng: DataTypes.STRING(64),
    name_native: DataTypes.STRING(64),
    sort: DataTypes.INTEGER,
    visible: DataTypes.BOOLEAN,
    available: DataTypes.BOOLEAN,
  });

  Language.listCodes = async function (lang, unit, init) {
    const data = await sequelize.query(
      'SELECT id FROM Languages WHERE available = 1',
    );

    if (!data[0]) {
      return 'Details are incorrect';
    }

    return data[0];
  };

  Language.listLangsData = async function (lang, unit, init) {
    const data = await sequelize.query(
      'SELECT id, visible FROM Languages WHERE available = 1',
    );

    if (!data[0]) {
      return 'Details are incorrect';
    }

    return data[0];
  };


  return Language;
};
