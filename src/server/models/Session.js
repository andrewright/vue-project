module.exports = (sequelize, DataTypes) => {
  const Session = sequelize.define('Session', {
    email: {
      type: DataTypes.STRING,
    },
    token: {
      type: DataTypes.TEXT,
    },
  });

  return Session;
};
