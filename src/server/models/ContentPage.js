// const {Content} = require('../models/')

module.exports = (sequelize, DataTypes) => {
  const ContentPage = sequelize.define('ContentPage', {
    page: {
      type: DataTypes.STRING(64),
      unique: 'unit_key',
    },
  });

  ContentPage.readPageData = async function (page) {
    try {
      const data = await sequelize.query(
        `SELECT cu.*, c.* FROM ContentPages cp LEFT JOIN ContentUnits cu ON cp.id = cu.page_id LEFT JOIN Content c ON cu.id = c.unit_id WHERE cp.page = '${page}' UNION SELECT cu.*, c.* FROM ContentUnits cu LEFT JOIN Content c ON cu.id = c.unit_id WHERE cu.page_id IS NULL`,
      );
      console.log('ContentPage.js:16', data);

      if (!data) {
        return 'Data is not available';
      }

      return data;
    } catch (err) {
      return false;
    }
  };

  return ContentPage;
};
