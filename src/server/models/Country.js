module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    code: DataTypes.STRING(2),
    name_eng: DataTypes.STRING(64),
    sort: DataTypes.INTEGER,
  });

  Country.listNames = async function () {
    const data = await sequelize.query(
      'SELECT * FROM Countries ORDER BY name_eng ASC',
    );

    if (!data[0]) {
      return 'Details are incorrect';
    }

    return data[0];
  };

  return Country;
};
