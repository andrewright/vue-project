const { sequelize, Content, ContentUnit, ContentPage } = require('../models');

const controller = {};

controller.readUnitKey = async (req, res) => {
  const { lang, unit, page,  key, init } = req.body;
  let resultContent = '';
  try {
    if (lang === '*') {
      resultContent = await sequelize.query(
        `SELECT * FROM Content c INNER JOIN ContentUnits cu ON cu.id = c.unit_id WHERE cu.unit = '${unit}' AND c.key = '${key}' AND c.init = '${init || false}'`,
      );
      console.log('ContentController.js:12', page);
    } else {
      resultContent = await sequelize.query(
        `SELECT * FROM Content c INNER JOIN ContentUnits cu ON cu.id = c.unit_id WHERE lang = '${lang}' AND cu.unit = '${unit}' AND c.key = '${key}' AND c.init = '${init || false}'`,
      );
    }
    if (!resultContent[0]) {
      return res.status(403)
        .send({
          error: 'Details are incorrect',
        });
    }
    res.send(resultContent[0]);
  } catch (err) {
    res.status(500)
      .send({
        error: 'Incorrect details',
        message: err.message,
      });
  }
  return true;
};

controller.readUnit = async (req, res) => {
  const { lang, unit, key, init } = req.body;
  try {
    res.send(await Content.readUnit(lang, unit, init));
  } catch (err) {
    res.status(500)
      .send({
        error: 'Incorrect details',
        message: err.message,
      });
  }
  return true;
};

controller.readPage = async (req, res) => {
  try {
    res.send(await ContentPage.readPageData(req.body.page));
  } catch (err) {
    res.status(500)
      .send({
        error: 'Incorrect details',
        message: err.message,
      });
  }
  return true;
};

controller.save = async (req, res) => {
  let data;
  let result;
  const { lang, unit, page, key, init, content } = req.body;
  const unitData = { unit };
  // Updating / creating if page is specified
  if (page) {
    const pageData = { page };
    result = await ContentPage.findOne({
      where: pageData,
    });
    if (!result) {
      try {
        result = await ContentPage.create(pageData);
      } catch (err) {
        // the page already exists, suppressing page unique error, do nothing
      }
    }
    if (!result) {
      result = await ContentPage.findOne({
        where: { page },
      });
    }
    // its guarateed result.id exists
    unitData.page_id = result.id;
  }
  try {
    // Updating / creating units for a content record
    result = await ContentUnit.findOne({
      where: unitData,
    });
  } catch (err) {
    res.status(400)
      .send({
        error: 'Something went wrong',
        err: err.message,
      });
  }
  try {
    if (!result) {
      result = await ContentUnit.create(unitData);
    }
  } catch (err) {
    result = await ContentUnit.findOne({
      where: unitData,
    });
  }
  try {
    data = {
      lang,
      unit_id: result.id,
      init: init || false,
      key,
    };
    result = await Content.findOne({
      where: data,
    });
    if (!result) {
      data.text = content;
      result = await Content.create(data);
    } else {
      result = await Content.update({ text: content }, { where: data });
    }
    res.send(result);
  } catch (err) {
    res.status(400)
      .send({
        error: 'Something went wrong',
        errors: err.errors,
      });
  }
};

/**
 * Main entry point of Content API
 * Usage: /content-sync/api/v1
 * @param req HTTP Request
 * @param res HTTP Response
 */
controller.contentManage = async (req, res) => {
  const { method } = req.body;
  switch (method) {
    case 'readPage':
      return controller.readPage(req, res);
    case 'readUnit':
      return controller.readUnit(req, res);
    case 'readUnitKey':
      return controller.readUnitKey(req, res);
    case 'save':
    case 'update':
      return controller.save(req, res);
    default:
      return false;
  }
};


module.exports = controller;
