/* const { sequelize } = require('../models'); */
const { Content } = require('../models');
const { render, prepareContext } = require('./Controller');

module.exports = {

  /**
   * Index action
   * @param req HTTP Request
   * @param res HTTP Response
   */
  async index(req, res) {
    const texts = {};

    Content.readUnit('en', 'content', 0)
      .then((data) => {
        prepareContext(data, texts);

        const context = {
          url: req.url,
          title: '$ssrContext.title in App.vue await response from server.js context data.',
          texts,
          meta: `
          <meta name='keywords' content='keywords' />
          <meta name="description" content="description" />
        `,
        };

        render(context, res);
      });
  },

};
