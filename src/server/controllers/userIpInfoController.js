const axios = require('axios');

module.exports = {
  async getUserIpInfo(req, res) {

    /*Fixme userIp  variable might be user ip address.*/
    const userIp = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
    console.log('userIpInfoController.js:10', userIp);
  
    /*axios.get('https://ipinfo.io/8.8.8.8/?15684db58401a7')*/
    axios.get('https://ipinfo.io/'+ userIp +'/?15684db58401a7')
      .then((response) => {
        res.status(200)
          .send({
            message: 'ok',
            ipinfo: response.data,
          });
      })
      .catch((error) => {
        res.status(500)
          .send({
            message: 'Internal error 500',
            error: error,
          });
      });
  },
};
