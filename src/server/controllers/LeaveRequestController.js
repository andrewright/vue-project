const request = require('request');

module.exports = {
  async sendRequestToCrm(req, res){
    console.log('==========================');
    console.log('Controller req:', req.body);

    const params = req.body;
    const options = {
      url:'https://webto.salesforce.com/servlet/servlet.WebToLead',
      qs: { encoding: 'UTF-8' },
      headers: {
        'cache-control': 'no-cache',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      form: {
        'oid': '00D24000000rIG6',
        'first_name': params.name || null,
        'phone': params.phone,
        'email': params.email,
        '00N2400000IgfgI': '', //ref.package ? ref.package : _self.getPurchases(),
        '00N2400000HsXuc': params.leadLanguage, //user's language (props params from router.js),
        '00N2400000IgeOr': params.requestPageUrl, //website source (page url)
        '00N2400000HsXub': params.cookieCountry, //user's country (ipinfo.io service)
        '00N2400000HtPST': '', // agent ID (WL service)
        '00N2400000Igdi2': params.cookieUtmSource, //utm_source (from cookie)
        '00N2400000Igdi7': params.cookieUtmMedium, //utm_medium (from cookie)
        '00N2400000IgdiC': params.cookieUtmCampaign, //utm_campaign (from cookie)
        '00N2400000IgdiH': params.cookieUtmTerm, //utm_term (from cookie)
        '00N2400000IgdiM': params.cookieUtmContent, //utm_content (from cookie)
        '00N2400000Igep9': params.cookieClientId, // clientID from _ga (from cookie) //($.cookie('_ga')) ? $.cookie('_ga').match(/(\d+\.\d+)$/)[0] : null,
      },
    };
    console.log('LeaveRequestController.js:35', options);

    request.post(options, (error, response, body) => {
      if (error || response.statusCode !== 200) {
        res.status(500)
          .send({
            error: {
              key: 'form',
              message: 'Your request wasn\'t sent!',
            },
          });
      } else {
        res.status(200)
          .send({
            success: {
              key: 'form',
              message: 'Your request was sent!',
            },
          });
      }
    });
  },
};
