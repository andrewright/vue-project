/* const { sequelize } = require('../models'); */
const { Language, Country } = require('../models');

module.exports = {

  /**
   * Index action
   * @param req HTTP Request
   * @param res HTTP Response
   */
  async languages(req, res) {
    try {
      const result = await Language.listCodes();
      const langs = [];
      result.forEach(el => langs.push(el.id));
      res.status(200).send(langs);
    } catch (err) {
      res.status(500)
        .send({
          error: 'Incorrect details',
        });
    }
  },

  /**
   * Countries
   * @param req HTTP Request
   * @param res HTTP Response
   */
  async countries(req, res) {
    try {
      const result = await Country.listNames();
      const countries = [];
      result.forEach(el => countries.push(el.name_eng));
      res.status(200).send(countries);
    } catch (err) {
      console.log(err.message);
      res.status(500)
        .send({
          error: 'Incorrect details',
        });
    }
  },

};
