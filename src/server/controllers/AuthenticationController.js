const path = require('path');

const projectRoot = path.resolve(__dirname, '../../..');
const { User } = require(path.join(projectRoot, '/src/server/models'));
const { Session } = require(path.join(projectRoot, '/src/server/models'));
const jwt = require('jsonwebtoken');

const config = require(path.join(projectRoot, '/config'));

module.exports = {
  async register(req, res) {
    try {
      const user = await User.create(req.body);
      res.send(user.toJSON());
    } catch (err) {
      res.status(400)
        .send({
          error: {
            key: 'email',
            message: 'This email account is already in use',
          },
        });
    }
  },
  async login(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({
        where: {
          email,
          confirmed: true,
        },
      });
      if (!user) {
        return res.status(403)
          .send({
            error: {
              key: 'email',
              message: 'Email is incorrect or access is not confirmed by administrator',
            },
          });
      }

      const isPasswordValid = await user.comparePassword(password);
      if (!isPasswordValid) {
        return res.status(403)
          .send({
            error: {
              key: 'password',
              message: 'Password is incorrect',
            },
          });
      }

      const userToJson = user.toJSON();
      const expire = config.authentication.jwtLifetime;
      const token = jwt.sign(
        userToJson,
        config.authentication.jwtSecret,
        { expiresIn: expire },
      );

      try {
        await Session.create({
          email,
          token,
        });
        res.send({
          user: userToJson,
          token,
        });
      } catch (err) {
        res.status(403)
          .send({
            error: {
              key: 'credentials',
              message: 'Token was not wrote to data base',
            },
          });
      }
    } catch (err) {
      res.status(500)
        .send({
          error: {
            key: 'credentials',
            message: 'Incorrect credentials or something went wrong',
          },
        });
    }
  },
  async authed(req, res) {
    try {
      const { token } = req.body;
      const session = await Session.findOne({
        where: {
          token,
        },
      });
      if (!session) {
        return res.status(200)
          .send({response:0});
      }
      return res.status(200)
        .send({response:'OK'});
    } catch (err) {
      res.status(500)
        .send({
          error: {
            key: 'credentials',
            message: 'Incorrect credentials or something went wrong',
          },
        });
    }
  },
};
