const path = require('path');

const projectRoot = path.resolve(__dirname, '../..');

const SiteController = require(path.join(projectRoot, '/src/server/controllers/SiteController'));
const AuthenticationController = require(path.join(projectRoot, '/src/server/controllers/AuthenticationController'));
const AuthenticationControllerPolicy = require(path.join(projectRoot, '/src/server/policies/AuthenticationControllerPolicy'));
const ContentController = require(path.join(projectRoot, '/src/server/controllers/ContentController'));
const ContentControllerPolicy = require(path.join(projectRoot, '/src/server/policies/ContentControllerPolicy'));
const LeaveRequestPolicy = require(path.join(projectRoot, '/src/server/policies/LeaveRequestPolicy'));
const LeaveRequestController = require(path.join(projectRoot, '/src/server/controllers/LeaveRequestController'));
const userIpInfoController = require(path.join(projectRoot, '/src/server/controllers/userIpInfoController'));


module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register);
  app.post('/login',
    AuthenticationControllerPolicy.login,
    AuthenticationController.login);
  app.post('/content-sync/api/v1',
    ContentControllerPolicy.contentManage,
    ContentController.contentManage);
  app.post('/authed',
    AuthenticationController.authed);
  app.get('/site-languages',
    SiteController.languages);
  app.get('/site-countries',
    SiteController.countries);
  app.post('/leave-request',
    LeaveRequestPolicy.checkFields,
    LeaveRequestController.sendRequestToCrm);
  app.get('/ipinfo',
    userIpInfoController.getUserIpInfo);
  app.get('/status', (req, res) => {
    res.send({
      message: 'hello world!',
    });
  });
};
