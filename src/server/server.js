const path = require('path');

const projectRoot = path.resolve(__dirname, '../..');
const express = require('express');

const server = express();
const cookiesMiddleware = require('universal-cookie-express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const { sequelize, Session } = require(path.join(projectRoot, '/src/server/models'));
const config = require(path.join(projectRoot, '/config'));
const { createBundleRenderer } = require('vue-server-renderer');

const serverBundle = require(path.join(projectRoot, '/dist/vue-ssr-server-bundle.json'));
const clientManifest = require(path.join(projectRoot, '/dist/vue-ssr-client-manifest.json'));
const template = require('fs').readFileSync('./src/server/index.template.html', 'utf-8');

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template,
  clientManifest,
});

server
  .use(morgan('combined'))
  // .use('/', express.static('./dist'))
  .use('/main.js', express.static('./dist/main.js'))
  .use('/commons.css', express.static('./dist/commons.css'))
  .use('/_vendors.css', express.static('./dist/_vendors.css'))
  .use('/commons.js', express.static('./dist/commons.js'))
  .use('/_vendors.js', express.static('./dist/_vendors.js'))
  .use('/assets', express.static('./assets'))
  .use('/assets/js/services/SiteInfoService.js', express.static('./src/services/SiteInfoService.js'))
  .use('/assets/js/services/ContentService.js', express.static('./src/services/ContentService.js'))
  .use('/assets/js/services/AuthenticationService.js', express.static('./src/services/AuthenticationService.js'))
  .use('/assets/js/contentManage.js', express.static('./src/scripts/contentManage.js'))
  .use(bodyParser.json())
  .use(cors());

require(path.join(projectRoot, '/src/server/routes'))(server);

let cookieValue;
let token;
server
  .use(cookiesMiddleware())
  .use((req, res, next) => {
    const paramsList = req.query;
    const paramsListKeys = Object.keys(req.query);
    const utmList = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'];
    const utmListInParams = paramsListKeys.filter(element => utmList.includes(element));
    function clearUtmUserCookies(utmListToDelete) {
      utmListToDelete.forEach((element) => {
        req.universalCookies.remove(element);
      });
    }
    function setUtmCookikes(listCookies, paramas) {
      listCookies.forEach((element) => {
        req.universalCookies.set(element, paramas[element]);
      });
    }
    if (utmListInParams.length) {
      clearUtmUserCookies(utmList);
      setUtmCookikes(utmListInParams, paramsList);
    } else {
      console.log('server.js:80', 'NO UTM MARKS');
    }
    next();
  })
.use((req, res, next) => {
    cookieValue = req.universalCookies.get('vuex') || {};
    token = cookieValue.token || null;
    next();
  })
  .use(async (req, res, next) => {
    if (token) {
      // Destroy all inactive tokens from Sessions table
      try {
        await Session.destroy({
          where: {
            createdAt: { // createdAt < NOW() - jwtLifetime
              lt: new Date(new Date() - config.authentication.jwtLifetime),
            },
          },
        });
      } catch (err) {
        console.log('Session destroy error!');
      }
      // Check current token from cookie with saved token from Sessions table
      try {
        const tokenExist = await Session.findOne({
          where: {
            token,
            /* createdAt: { //createdAt >= NOW() - jwtLifetime
              gte: new Date(new Date - config.authentication.jwtLifetime)
            } */
          },
        });
        if (!tokenExist) {
          token = null;
          console.log('Session token doesn\'t exist!');
        } else {
          console.log('Session token is valid!');
        }
        console.log('Session token is valid!');
      } catch (err) {
        token = null;
        console.log('Session token error!');
      }
    }
    next();
  });

server.get('*', (req, res) => {
  const context = {
    url: req.url,
    /* FIXME Do we need title and Meta?! */
    title: '$ssrContext.title in App.vue await response from server.js context data.',
    auth: !!token,
    token,
    meta: `
      <meta name='keywords' content='keywords' />
      <meta name="description" content="description" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
    `,
  };

  renderer.renderToString(context, (err, html) => {
    if (err) {
      if (err.code === 404) {
        console.log('Error render to string: ', err);
        res.status(err.code)
          .end('Page not found');
      } else {
        console.log('Error rendering to string: ', err);
        res.status(err.code)
          .end('Internal server error');
      }
      return;
    }
    res.end(html);
  });
});

sequelize.sync()
  .then(() => {
    server.listen(config.port);
    console.log(`Application strated on port ${config.port}`);
  });
