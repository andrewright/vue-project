const Joi = require('joi');

module.exports = {
  checkFields(req, res, next) {
    console.log('Policy req:', req.body);
    const schema = {
      name: Joi.any(),
      company: Joi.any(),
      leadLanguage: Joi.any(),
      requestPageUrl: Joi.any(),
      email: Joi.string()
        .email().required(),
      phone: Joi.string()
        .regex(
          new RegExp('^[\\+][0-9]{1,3}[0-9( )-]{7,15}$'),
        ),
      terms: Joi.boolean()
        .invalid(false),
      cookieCountry: Joi.any(),
      cookieClientId: Joi.any(),
      cookieUtmCampaign: Joi.any(),
      cookieUtmContent: Joi.any(),
      cookieUtmMedium: Joi.any(),
      cookieUtmSource: Joi.any(),
      cookieUtmTerm: Joi.any(),
    };

    const { error } = Joi.validate(req.body, schema);

    if (error) {
      console.log('JOI:Error', error);
      switch (error.details[0].context.key) {
        case 'name':
        case 'company':
        case 'leadLanguage':
        case 'cookieCountry':
        case 'cookieClientId':
        case 'cookieUtmCampaign':
        case 'cookieUtmContent':
        case 'cookieUtmMedium':
        case 'cookieUtmSource':
        case 'cookieUtmTerm':
          res.status(422)
            .send({
              error: {
                key: error.details[0].context.key,
                message: 'Data type not correct',
              },
            });
          break;
        case 'email':
          res.status(422)
            .send({
              error: {
                key: error.details[0].context.key,
                message: 'You must provide a valid email address',
              },
            });
          break;
        case 'phone':
          res.status(422)
            .send({
              error: {
                key: error.details[0].context.key,
                message: 'You must provide a valid password which can contain <strong>only latin letters and numbers</strong>',
              },
            });
          break;
        case 'terms':
          res.status(422)
            .send({
              error: {
                key: error.details[0].context.key,
                message: 'You must agree all terms and conditions',
              },
            });
          break;
        default:
          res.status(422)
            .send({
              error: {
                key: error.details[0].context.key,
                message: 'Something went wrong',
                data: req.body,
              },
            });
      }
    } else {
      next();
    }
  },
};
