// const Joi = require('joi')

const { Session } = require('../models');

/**
 * Content
 * @param lang - landuage 2-letter code
 * @param unit unit name - group of values used for sections or
 *    other parts of page
 * @param init Boolean - whether a content record is initialising,
 *    first time used
 * @param content content string
 * @param key key tag for content records identification
 *
 */

/**
 * Defining Policy object
 */
const policy = {};


/**
 * Validation methods
 */
policy.validateSave = async () => {
  let errorParam = false;
  const { req, next } = policy;
  const { lang, unit, key, init, content, token } = req.body;
  if (lang.match(/[a-z]{2}/) === false) {
    errorParam = 'Language';
  }
  if (unit.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Unit';
  }
  if (key && key.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Key';
  }
  if (init && init.match(/[0-1]{1}/) === false) {
    errorParam = 'Init';
  }
  if (content && content.match(/[{}^~\\]+/)) {
    errorParam = 'Content';
  }
  if (token) {
    const session = await Session.findOne({
      where: {
        token,
      },
    });
    if (!session) {
      errorParam = 'Auth Token';
    }
  } else {
    errorParam = 'Auth Token';
  }
  if (errorParam) {
    policy.validate(errorParam);
    return false;
  } return next();
};

policy.validateUnitKey = () => {
  let errorParam = false;
  const { req, next } = policy;
  const { lang, unit, key, init } = req.body;
  if (lang.match(/[a-z]{2}/) === false) {
    errorParam = 'Language';
  }
  if (unit.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Unit';
  }
  if (key && key.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Key';
  }
  if (init && init.match(/[0-1]{1}/) === false) {
    errorParam = 'Init';
  }
  if (errorParam) {
    policy.validate(errorParam);
    return false;
  } return next();
};

policy.validateUnit = () => {
  let errorParam = false;
  const { req, next } = policy;
  const { lang, unit, init } = req.body;
  if (lang && lang.match(/[a-z]{2}/) === false) {
    errorParam = 'Language';
  }
  if (unit && unit.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Unit';
  }
  if (init && init.match(/[0-1]{1}/) === false) {
    errorParam = 'Init';
  }
  if (errorParam) {
    policy.validate(errorParam);
    return false;
  } return next();
};

policy.validatePage = () => {
  let errorParam = false;
  const { req, next } = policy;
  const { page } = req.body;
  if (page == undefined || page.match(/[<>{}=#^!~;*%$@&+\\]+/)) {
    errorParam = 'Page';
  }
  if (errorParam) {
    policy.validate(errorParam);
    return false;
  } return next();
};

policy.validate = (errorParam) => {
  const { res } = policy;
  res.status(200)
    .send({
      error: `Bad ${errorParam} parameter`,
    });
};

/**
 * Main entry point of Content API
 * Usage: /content-sync/api/v1
 * @param req HTTP Request
 * @param res HTTP Response
 */
policy.contentManage = (req, res, next) => {
  policy.req = req;
  policy.res = res;
  policy.next = next;
  const { method } = req.body;
  switch (method) {
    case 'readPage':
      return policy.validatePage();

    case 'readUnit':
      return policy.validateUnit();

    case 'readUnitKey':
      return policy.validateUnitKey();

    case 'save':
    case 'update':
      return policy.validateSave();

    default:
      return false;
  }
};

module.exports = policy;
