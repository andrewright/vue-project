// entry-server.js
import { createApp } from './app';
import ContentService from './services/ContentService';

// since there could potentially be asynchronous route hooks or components,
// we will be returning a Promise so that the server can wait until
// everything is ready before rendering.
export default context => new Promise((resolve, reject) => {
  const { app, router, store, meta } = createApp();

  // set server-side router's location
  router.push(context.url);
  // set meta
  context.meta = meta;

  // prepare page name
  let pageName = 'home';
  try {
    const $arr = router.history.current.name.split('-');
    $arr.pop(); pageName = $arr.join('-');
  } catch (err) {
    // suppress split error
  }

  ContentService.readPage({
    page: pageName,
  }).then((response) => {
    const contents = response.data;
    let contentData = [];
    if (typeof contents === 'object' && contents.length >= 1) {
      contentData = contents.pop();
    }
    // wait until router has resolved possible async components and hooks
    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents();
      // no matched routes, reject with 404
      if (!matchedComponents.length) {
        return reject(new Error({ message: 'entry-server.js ', code: 404 }));
      }
      let lang = '*';
      try {
        lang = router.history.current.matched[0].props.default.language;
      } catch (err) {
        console.log(err);
      }
      // call `asyncData()` on all matched route components
      Promise.all(matchedComponents.map((Component) => {
        if (Component.asyncData) {
          return Component.asyncData({
            store,
            lang,
            route: router.currentRoute,
            contents: contentData,
          });
        }
        return false;
      })).then((data) => {
        // After all preFetch hooks are resolved, our store is now
        // filled with the state needed to render the app.
        // When we attach the state to the context, and the `template` option
        // is used for the renderer, the state will automatically be
        // serialized and injected into the HTML as `window.__INITIAL_STATE__`.
        if (typeof data[0] !== 'undefined') {
          store.state.contents = data[0].contents;
          store.state.lang = data[0].lang;
        }
        store.state.auth = context.auth;
        store.state.token = context.token;
        context.state = store.state;
        // the Promise should resolve to the app instance so it can be rendered
        resolve(app);
      }).catch(reject);
      return false;
    }, reject);
    return false;
  });
}).catch((error) => {
  console.log(error);
});
