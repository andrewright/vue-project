import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import VueScrollTo from 'vue-scrollto';
import BootstrapVue from 'bootstrap-vue';
import Vuelidate from 'vuelidate';
import * as VueGoogleMaps from 'vue2-google-maps';
import App from './App.vue';
import { createRouter } from './router/router';
import { createStore } from './store/store';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(VueScrollTo, {
  duration: 800,
  easing: 'ease-in-out',
  offset: -60,
});
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCVGHrnD8s8Lh3T_z7rwYGsUOvMoDN_nEQ',
  },
});

// export a factory function for creating fresh app, router and store
// instances
export function createApp() {
  // create router instance
  const router = createRouter();

  // create store instance
  const store = createStore();
  sync(store, router);

  const app = new Vue({
    // the root instance simply renders the App component.
    router,
    store,
    render: h => h(App),
  });

  // create meta instance
  const meta = app.$meta();

  // return the app, router and store
  return {
    app,
    router,
    store,
    meta,
  };
}
