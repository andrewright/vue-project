// const path = require('path');
// const projectRoot = '../..';
// const config = require(path.join(projectRoot, '/config.js'));

import axios from 'axios';

let url = '';

if (!process.browser) {
  const config = require('../../config.js');

  url = `${config.api.protocol}://${config.api.host}:${config.api.port}/`;
} else {
  url = 'http://localhost:8081';
}

export default () => axios.create({
  baseURL: url,
});
