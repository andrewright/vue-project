import Api from './Api.js';

export default {
  update(data) {
    data.method = 'save';
    const response = Api()
      .post('content-sync/api/v1', data);
    return response;
  },
  readPage(data) {
    data.method = 'readPage';
    const response = Api()
      .post('content-sync/api/v1', data);
    return response;
  },
  readUnit(data) {
    data.method = 'readUnit';
    const response = Api()
      .post('content-sync/api/v1', data);
    return response;
  },
  readUnitKey(data) {
    data.method = 'readUnitKey';
    const response = Api()
      .post('content-sync/api/v1', data);
    return response;
  },
};
