import Api from '../services/Api.js';

export default {
  userIpInfo() {
    return Api()
      .get('ipinfo');
  }
};
