import Api from '../services/Api.js';

export default {
  languages() {
    return Api()
      .get('site-languages');
  },
  countries() {
    return Api()
      .get('site-countries');
  },
};
