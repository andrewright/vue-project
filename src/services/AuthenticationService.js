import Api from '../services/Api.js';

export default {
  register(credetials) {
    return Api()
      .post('register', credetials);
  },
  login(credetials) {
    return Api()
      .post('login', credetials);
  },
  authed(token) {
    return Api()
      .post('authed', token);
  },
};
