-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Languages`;
CREATE TABLE `Languages` (
  `id` varchar(2) NOT NULL,
  `name_eng` varchar(64) DEFAULT NULL,
  `name_native` varchar(64) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `Languages` (`id`, `name_eng`, `name_native`, `sort`, `visible`, `available`, `createdAt`, `updatedAt`) VALUES
('en',	'English',	'English',	1,	1,	1,	'2018-10-26 16:08:46',	'2018-10-26 16:08:46'),
('pl',	'Polish',	'Polski',	2,	1,	1,	'2018-10-26 16:09:19',	'2018-10-26 16:09:19'),
('nl',	'Dutch',	'Nederlands',	3,	1,	1,	'2018-10-26 16:09:46',	'2018-10-26 16:09:46'),
('cz',	'Chech',	'Čeština',	4,	1,	1,	'2018-10-26 16:10:40',	'2018-10-26 16:10:40'),
('ee',	'Estonian',	'Eesti',	5,	1,	1,	'2018-10-26 16:11:33',	'2018-10-26 16:11:33'),
('lt',	'Lithuanian',	'Lietuvių',	6,	1,	1,	'2018-10-26 16:12:07',	'2018-10-26 16:12:07'),
('lv',	'Latvian',	'Latviešu',	7,	1,	1,	'2018-10-26 16:12:39',	'2018-10-26 16:12:39');

-- 2018-12-04 09:24:12
