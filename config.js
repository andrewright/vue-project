require('dotenv')
  .config();

module.exports = {
  protocol: process.env.APP_PROTOCOL,
  host: process.env.APP_HOST,
  port: process.env.APP_PORT,
  db: {
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    options: {
      dialect: process.env.DB_CONNECTION,
      host: process.env.DB_HOST,
      define: {
        engine: 'MyISAM',
        charset: 'utf8',
        collate: 'utf8_general_ci',
        timestamps: true,
      },
    },
  },
  api: {
    protocol: process.env.API_PROTOCOL,
    host: process.env.API_HOST,
    port: process.env.API_PORT,
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET,
    jwtLifetime: process.env.JWT_LIFETIME,
  },
};
