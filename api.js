export function fetchItem() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // переведёт промис в состояние fulfilled с результатом
      resolve({
        1: { title: 'title1' },
        2: { title: 'title2' },
      });
    }, 100);
  });
}
