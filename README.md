# Welcome to vue-project
Project for Dzinga copany. List of translatable landing pages.

## Installation
 - Install all packages
`$ npm install`
 - Start project
`$ npm start`
 - Run lint testing
 `$ npm run eslint`
